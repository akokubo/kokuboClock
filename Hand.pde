// 針クラス
class Hand {
  // 幾何変数
  float r;

  // 描画変数
  color stroke;
  float weight;

  Hand(float r, color stroke, float weight) {
    this.r = r;
    this.stroke = stroke;
    this.weight = weight;
  }

  void display(float degree) {
    // 角度をラジアンに変換
    float angle = radians(degree * 360 - 90);

    stroke(this.stroke);
    strokeWeight(weight);
    line(clock.x, clock.y, clock.x + r * cos(angle), clock.y + r * sin(angle));
  }
}