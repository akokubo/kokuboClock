// 時計オブジェクト
Clock clock;

void setup() {
  size(640, 360);

  // 時計オブジェクトの生成
  clock = new Clock();
}

void draw() {
  // 背景を黒に
  background(0);
  
  // 時計オブジェクトの描画
  clock.display();
}