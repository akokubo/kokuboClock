// 時計クラス
class Clock {
  // 幾何変数
  float x;
  float y;
  float r;

  // 描画変数
  float stroke;
  float strokeWeight;

  // 針
  Hand hourHand;
  Hand minuteHand;
  Hand secondHand;

  Clock() {
    // 幾何変数の初期化
    float margin = 20;
    x = width / 2;
    y = height / 2;
    r = min(x, y) - margin;

    // 描画変数の初期化
    this.stroke = 128;
    this.strokeWeight = 5;

    // 針の初期化
    hourHand = new Hand(0.6 * r, color(255, 0, 0), 10);
    minuteHand = new Hand(0.8 * r, color(0, 255, 0), 5);
    secondHand = new Hand(0.9 * r, color(0), 1);
  }

  // 描画
  void display() {
    // 文字盤の描画
    stroke(this.stroke);
    strokeWeight(this.strokeWeight);
    ellipse(x, y, 2 * r, 2 * r);

    // 針の描画
    hourHand.display((hour() % 12 + minute() / 60.0) / 12);
    minuteHand.display((minute() + second() / 60.0) / 60);
    secondHand.display(second() / 60.0);
  }
}